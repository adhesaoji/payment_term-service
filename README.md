1.To build payment service use - mvn clean install -DskipTests
2.To run payment service use- mvn spring-boot:run
3.To see service is running or not use - "http://localhost8080/"
4.To perform rest call can use swagger-ui- "http://localhost:8080/swagger-ui.html#/"