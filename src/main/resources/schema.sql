DROP TABLE IF EXISTS save_payment;  
CREATE TABLE save_payment (  
id BIGINT AUTO_INCREMENT  PRIMARY KEY,  
code VARCHAR(50) NOT NULL, 
description VARCHAR(500) NOT NULL, 
days INT(8) NOT NULL,
creation_date DATE,
remind_before_days INT(8) NOT NULL
);