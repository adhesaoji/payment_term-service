package com.trimind;

import org.apache.log4j.BasicConfigurator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PaymentTermApplication {

	public static void main(String[] args) {
		SpringApplication.run(PaymentTermApplication.class, args);
		BasicConfigurator.configure();
	}

}
