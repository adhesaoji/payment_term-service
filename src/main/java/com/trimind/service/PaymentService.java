package com.trimind.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.trimind.service.dto.PaymentDto;

public interface PaymentService {

	@Transactional
	PaymentDto createPaymentTerm(String code, String description, int days, int remindBeforeDays);

	@Transactional(readOnly = true)
	List<PaymentDto> getAllPaymentTerm();

	@Transactional(readOnly = true)
	List<PaymentDto> getPaymentTermByCode(String code);

	@Transactional(readOnly = true)
	PaymentDto getPaymentTermById(Long id);

	@Transactional
	PaymentDto updatePaymentTerm(Long id, String code, String description, String creatonDate, int days,
			int remindBeforeDays);

	@Transactional
	void deletePaymentTermByCode(String code) throws Exception;

	@Transactional
	void deletePaymentTermById(Long id) throws Exception;

}
