package com.trimind.service;

import static java.util.stream.Collectors.toList;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.trimind.domain.PaymentRepository;
import com.trimind.domain.SavePayment;
import com.trimind.service.dto.PaymentDto;

@Service
public class PaymentServiceImpl implements PaymentService {

	private final PaymentRepository paymentRepository;
	private final ModelMapper modelMapper;

	@Autowired
	public PaymentServiceImpl(PaymentRepository paymentRepository, ModelMapper modelMapper) {
		super();
		this.paymentRepository = paymentRepository;
		this.modelMapper = modelMapper;
	}

	@Override
	@Transactional
	public PaymentDto createPaymentTerm(String code, String description, int days, int remindBeforeDays) {
		SavePayment savePayment = new SavePayment();
		savePayment.setCode(code);
		savePayment.setDescription(description);
		savePayment.setDays(days);
		savePayment.setRemindBeforeDays(remindBeforeDays);
		Date date = new Date();
		savePayment.setCreationDate(date);

		savePayment = paymentRepository.save(savePayment);

		return modelMapper.map(savePayment, PaymentDto.class);

	}

	@Override
	public List<PaymentDto> getAllPaymentTerm() {
		List<SavePayment> allPaymentTerms = paymentRepository.findAll();

		return allPaymentTerms.stream().map(pterm -> modelMapper.map(pterm, PaymentDto.class)).collect(toList());
	}

	@Override
	public List<PaymentDto> getPaymentTermByCode(String code) {
		List<SavePayment> allPaymentTerms = paymentRepository.findAllByCode(code);

		return allPaymentTerms.stream().map(pterm -> modelMapper.map(pterm, PaymentDto.class)).collect(toList());
	}

	@Override
	public PaymentDto getPaymentTermById(Long id) {
		Optional<SavePayment> PaymentTerms = paymentRepository.findOneById(id);

		return modelMapper.map(PaymentTerms, PaymentDto.class);
	}

	@Override
	public PaymentDto updatePaymentTerm(Long id, String code, String description, String creatonDate, int days,
			int remindBeforeDays) {
		SavePayment savePayment = new SavePayment();
		savePayment.setId(id);
		savePayment.setCode(code);
		savePayment.setDescription(description);
		savePayment.setDays(days);
		savePayment.setRemindBeforeDays(remindBeforeDays);
		Date date1 = null;
		try {
			date1 = new SimpleDateFormat("yyyy-MM-dd").parse(creatonDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		savePayment.setCreationDate(date1);

		savePayment = paymentRepository.save(savePayment);

		return modelMapper.map(savePayment, PaymentDto.class);
	}

	@Override
	public void deletePaymentTermByCode(String code) throws Exception {
		List<SavePayment> sv = paymentRepository.findAllByCode(code);
		if (sv.isEmpty()) {
			throw new Exception("No document found with the specified document code");
		}

		try {
			paymentRepository.deleteAll(sv);
		} catch (DataAccessException e) {
			throw new Exception("An error occurred while attempting to delete a document");
		}
	}

	@Override
	public void deletePaymentTermById(Long id) throws Exception {

		SavePayment sv = paymentRepository.findOneById(id).orElseThrow(() -> {
			return new Exception("No document found with the specified document ID");
		});

		try {
			paymentRepository.delete(sv);
		} catch (DataAccessException e) {
			throw new Exception("An error occurred while attempting to delete a document");
		}
	}

}
