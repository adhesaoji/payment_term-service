package com.trimind.service.dto;

import java.util.Date;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PaymentDto {
	@NotEmpty
	private Long id;

	@NotEmpty
	private String code;

	@NotEmpty
	private String description;

	@NotEmpty
	private Date creationDate;

	@NotEmpty
	private int days;

	@NotEmpty
	private int remindBeforeDays;
}
