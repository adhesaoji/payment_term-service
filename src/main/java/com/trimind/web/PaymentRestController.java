package com.trimind.web;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.trimind.service.PaymentService;
import com.trimind.service.dto.PaymentDto;

import lombok.extern.log4j.Log4j;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
@Log4j
@RestController
@RequestMapping("/payment")
public class PaymentRestController {
	private final PaymentService paymentService;

	@Autowired
	public PaymentRestController(PaymentService paymentService) {
		this.paymentService = paymentService;
	}

	@PostMapping
	@ResponseStatus(HttpStatus.OK)
	public PaymentDto createPaymentTearm(@RequestParam(value = "code") String code,
			@RequestParam(value = "description") String description, @RequestParam(value = "days") int days,
			@RequestParam(value = "remindBeforeDays") int remindBeforeDays) {
			log.info("Creating Payment_Term.....");
		return paymentService.createPaymentTerm(code, description, days, remindBeforeDays);

	}

	@GetMapping
	@ResponseStatus(HttpStatus.OK)
	public List<PaymentDto> getAllPaymentTearm() {
		log.info("Retriving AllPayment_Term ");
		
		return paymentService.getAllPaymentTerm();
	}

	@GetMapping("code/{code}")
	@ResponseStatus(HttpStatus.OK)
	public List<PaymentDto> getPaymentTearmByCode(@PathVariable String code) {
		log.info("Retriving Payment_Term By Code - "+code);
		return paymentService.getPaymentTermByCode(code);
	}

	@GetMapping("id/{id}")
	@ResponseStatus(HttpStatus.OK)
	public PaymentDto getPaymentTearmById(@PathVariable Long id) {
		log.info("Retriving Payment_Term By Id - "+id);
		return paymentService.getPaymentTermById(id);
	}

	@PutMapping
	@ResponseStatus(HttpStatus.OK)
	public PaymentDto updatePaymentTearm(@RequestParam(value = "id") Long id, @RequestParam(value = "code") String code,
			@RequestParam(value = "description") String description,
			@RequestParam(value = "creationDate") String creatonDate, @RequestParam(value = "days") int days,
			@RequestParam(value = "remindBeforeDays") int remindBeforeDays) {
		log.info("Retriving Payment_Term By Id - "+id);
		return paymentService.updatePaymentTerm(id, code, description, creatonDate, days, remindBeforeDays);

	}

	@DeleteMapping("code/{code}")
	@ResponseStatus(HttpStatus.OK)
	public void deletePaymentTearmByCode(@PathVariable String code) throws Exception {
		log.info("Deleting Payment_Term By Code - "+code);
		paymentService.deletePaymentTermByCode(code);

	}

	@DeleteMapping("id/{id}")
	@ResponseStatus(HttpStatus.OK)
	public void deletePaymentTearmById(@PathVariable Long id) throws Exception {
		log.info("Deleting Payment_Term By Id - "+id);
		paymentService.deletePaymentTermById(id);

	}

}
