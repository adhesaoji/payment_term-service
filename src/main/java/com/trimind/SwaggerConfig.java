/**
 * 
 */
package com.trimind;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author VM
 *
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {
	/**
	 *
	 * @return Docket
	 */
	@Bean
	public Docket productApi() {

		return new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors.basePackage(("com.trimind.web"))).paths(PathSelectors.any()).build()
				.apiInfo(apiInfo());

	}

	/**
	 *
	 * @return ApiInf
	 */
	private ApiInfo apiInfo() {
		return new ApiInfoBuilder().title("Payment_Term Service").description("")
				.termsOfServiceUrl("https://trimind.payment_term.com/")
				.contact(new Contact("Developers", "https://projects.spring.io/spring-boot/", ""))
				.license("Open Source").licenseUrl("\"https://www.apache.org/licenses/LICENSE-2.0").version("1.0.0")
				.build();

	}

}
