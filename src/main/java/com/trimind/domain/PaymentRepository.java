package com.trimind.domain;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface PaymentRepository extends JpaSpecificationExecutor<SavePayment>, JpaRepository<SavePayment, Long> {
	List<SavePayment> findAllByCode(String code);

	Optional<SavePayment> findOneById(Long id);

	Optional<SavePayment> findOneByCode(String code);

}