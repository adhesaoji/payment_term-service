package com.trimind.domain;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Data
public class SavePayment {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@NotEmpty
	private String code;

	@NotEmpty
	private String description;

	@NotEmpty
	@Temporal(TemporalType.DATE)
	private Date creationDate;

	@NotEmpty
	private int days;

	@NotEmpty
	private int remindBeforeDays;

}