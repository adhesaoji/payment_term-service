package com.trimind;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WelcomePage {
	
	@GetMapping("/")
	@ResponseBody
	public String welcome() {
		return "<h1>Welcome to Payment_Term-Service of Trimind Tech Solution</h1>";
	}

}